from django.test import TestCase, Client
from django.urls import resolve
from datetime import datetime
from .forms import StatusForm
from django.http import HttpRequest
from .views import index
from .models import Status
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.

class story6(TestCase):

    def test_landing_page(self):
        response = Client().get("")
        self.assertEqual(response.status_code,200)

    def test_landing_page_template(self):
        response = Client().get("")
        self.assertTemplateUsed(response,'story_enam.html')

    def test_welcome_text(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Hello World",html_response)

    def test_models(self):
        Status.objects.create(status="abc", time =datetime.now())
        n = Status.objects.all().count()
        self.assertEqual(n,1)

    def test_form_valid(self):
        data = {'status':"abc","time": datetime.now()}
        status_form = StatusForm(data=data)
        self.assertTrue(status_form.is_valid())
        self.assertEqual(status_form.cleaned_data['status'],"abc")

    def test_form_post(self):
        test_str = 'abc'
        response_post = Client().post('', {'status':test_str,'time':datetime.now()})
        self.assertEqual(response_post.status_code,200)
        status_form = StatusForm(data={'status':test_str,'time':datetime.now()})
        self.assertTrue(status_form.is_valid())
        self.assertEqual(status_form.cleaned_data['status'],"abc")


