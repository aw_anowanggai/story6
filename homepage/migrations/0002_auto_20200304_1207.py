# Generated by Django 3.0.3 on 2020-03-04 05:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='status',
            field=models.CharField(max_length=300),
        ),
        migrations.AlterField(
            model_name='status',
            name='time',
            field=models.DateTimeField(),
        ),
    ]
